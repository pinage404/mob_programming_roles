# Mob Programming Roles

---

```mermaid
%%{ init: { 'theme':'dark', 'flowchart': { 'curve': 'basis' } } }%%
flowchart RL
    %% relations

    subgraph Group[The Group]
        subgraph Pair[The Pair]
            Driver -->|ask when don't understand what to do| Navigator
            Navigator -->|"give (highest level) instructions"| Driver
            Navigator -->|decide what to do| Navigator
        end

        Navigator --->|ask the next steps\n ask for ideas| Mobbers
        Mobbers --->|suggest ideas| Navigator

        Mobbers -->|talk about the next steps| Mobbers

        ToDoers

        Researchers

        Hoster
    end

    subgraph Platform[The Platform]
        subgraph Solution[The Solution]
            Code
        end
    end

    Group -->|read| Code
    Driver --->|write| Code

    Group --->|co-create| Solution

    Hoster -->|host| Platform

    Researchers -->|search| Documentations

    Group ---->|read| ToDoList
    ToDoers -->|fill & update| ToDoList

    subgraph Facilitators
        Organizers
        Sponsors
        InteruptionProtectors
        TimeKeepers
    end

    Facilitators -->|take care of the well-being of| Group

    Organizers -->|collect the feelings| Group
    Organizers -->|propose organizational methods| Group

    Sponsors -->|encourage the less privileged| Group
    Sponsors -->|reward good behaviors| Group

    InteruptionProtectors -->|protect from interuption| Group

    TimeKeepers -->|encourage taking breaks| Group
    TimeKeepers -->|timebox| Group
    TimeKeepers -->|turn the roles| Group
    TimeKeepers -->|are responsible of| Timer

    subgraph Legend
        direction RL

        Bonus -->|action on| Required
        Bonus -->|"action (ideally) on"| Required
    end

    %% styling

    Code{{"The Code {}"}}
    Hoster["The Hoster 🧑‍🔧"]
    Documentations>"Documentations 📖"]
    Driver["The Driver ⌨️"]
    ToDoList>"The (shared) ToDo List 📝"]
    ToDoers["ToDoers ✍️"]
    Timer(("The\n (shared)\n Timer\n⏳"))
    Sponsors["Sponsors 🙌"]
    Navigator["The Navigator 🔀"]
    Researchers["Researchers 🕵️"]
    Mobbers["Mobbers 💡"]
    Required[Required Role]
    Bonus[Bonus Role]

    classDef Required fill:royalblue,color:black,stroke:black
    class Navigator,Driver,Mobbers,Required Required

    classDef Bonus fill:#994D00,color:white,stroke:white
    class Organizers,Sponsors,InteruptionProtectors,TimeKeepers,ToDoers,Researchers,Hoster,Bonus Bonus

    classDef Hidden visibility:hidden
    %% class Group,Mobbers Hidden
    %% class Solution Hidden
    %% class Hoster,Platform Hidden
    %% class Facilitators Hidden
    %% class Organizers Hidden
    %% class Sponsors Hidden
    %% class InteruptionProtectors Hidden
    %% class TimeKeepers,Timer Hidden
    %% class ToDoers,ToDoList Hidden
    %% class Researchers,Documentations Hidden
```
