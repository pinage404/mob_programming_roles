---
title: Mob Programming
verticalSeparator: ----
---

# Mob Programming

101

A.K.A. _Ensemble Programming_

A.K.A. _Software Teaming_

---

## Objective

Align everyone with the mob basics

---

<style>
.toc-2-columns ul {
    column-count: 2;
}
</style>
<div class="toc-2-columns">

- [Objective](#objective)
- [What ?](#what-)
- [Why ?](#why-)
- [Who ?](#who-)
- [How many ?](#how-many-)
- [How long ?](#how-long-)
- [When ?](#when-)
- [Where ?](#where-)
- [Stuck ?](#stuck-)
- [When not ?](#when-not-)
- [How ?](#how-)
- [Tools](#tools)
- [External Resources](#external-resources)

</div>

---

## What ?

----

### Definition

<span class="fragment">💻 + 👥</span>
<span class="fragment">+ 👤</span>
<span class="fragment">+ 👤</span>
<span class="fragment">+ 👤</span>
<span class="fragment">…</span>

* **A group** of people (idealy an entire team) <!-- .element: class="fragment" -->
* trying to solve the **same problem** <!-- .element: class="fragment" -->
* in the **same time** <!-- .element: class="fragment" -->
* in the **same space** <!-- .element: class="fragment" -->
* using a **single computer** 💻 <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    also good for pair programming

----

### Origins

* 2011 invented at Hunter
* 2014 described by Woody Zuill from Hunter

---

## Why ?

Use Collective Intelligence

<style>
@keyframes brillant {
    to {
        text-shadow: 0 0 0.3em;
    }
}

.brillant {
    animation-name: brillant;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-direction: alternate-reverse;
}

.idea {
    color: yellow;
}

.poor.idea {
    font-size: 0.6em;
    letter-spacing: 2rem;
    vertical-align: middle;
    opacity: 0.8;
}

.brillant.idea {
    font-style: normal;
}
</style>

🧠 🟰 <small class="poor idea">💡</small>

🧠 ➕ 🧠 🟰 <em class="brillant idea">💡</em> ✖️ <em class="brillant idea">💡</em>

----

### Share

* Knowledge <!-- .element: class="fragment" -->
* Best Practices <!-- .element: class="fragment" -->
* Tools <!-- .element: class="fragment" -->
* Methodologies <!-- .element: class="fragment" -->

----

### Learn

* together <!-- .element: class="fragment" -->
* from each other <!-- .element: class="fragment" -->

----

### Improve

* Code quality <!-- .element: class="fragment" -->
* Collective code ownership <!-- .element: class="fragment" -->
* Collaboration <!-- .element: class="fragment" -->
* Cohesion <!-- .element: class="fragment" -->
* Team members levels 📊 <!-- .element: class="fragment" -->
* [Truck Factor](https://www.agileadvice.com/2005/05/15/agilemanagement/truck-factor/) robustness 🚛
  * Maintainability <!-- .element: class="fragment" -->

note:
    harmonizes levels upwards

----

### Do

instead of planning to do

🧑‍💻 > 🗺️

note:
  avoid :
  waste (lean) ;
  context switching ;
  async ping pong ;

---

## Who ?

The people who want to

Ideally, the whole team (not only the dev)

---

## How many ?

Between 3 and 8

5~6 is the best (for me)

8+ maybe you should split

---

## How long ?

1h minimum

2h is good

---

## When ?

When people want to

Ideally, most of the work time

note:
  avoid :
  meetings ;
  context switching ;
  waste (lean) ;
  async ping pong ;

---

## Where ?

In a quiet room

* In the same room : easiest 😀 <!-- .element: class="fragment" -->
* Remote : easy 🙂
  * need good connexion <!-- .element: class="fragment" -->
* Hybrid : harder 😒
  * often not cool for the remote ones
    * connexion issues
    * face-to-face people speaking at the same time <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    🎤

---

## Stuck ?

Vote !

----

3️⃣

🤜 🤜 🤜 🤜

----

2️⃣

🤜 🤜 🤜 🤜

----

1️⃣

🤜 🤜 🤜 🤜

----

👍 👍 🫱 👎

note:
    👎 ask ➡️ 🫱

---

## When not ?

Tasks with a very long feedback loop

Unclear problem

note:
  build in CI

---

## How ?

----

### Live Coding 🧑‍🏫

🧑‍💻

* 😴 <!-- .element: class="fragment" -->
* 📱 <!-- .element: class="fragment" -->
* 😖 <!-- .element: class="fragment" -->
* 🗣️👤💬🌄 <!-- .element: class="fragment" -->

🙁 <!-- .element: class="fragment" -->

----

### Strong Style 👥

----

#### Driver ⌨️

<div class="fragment">

**Types** _only_ what the Navigator 🔀 requests

`Idea 💡 -> Code {}`

</div>

----

#### Navigator 🔀

<div class="fragment">

**Chooses an idea** (from the Mobbers 💡 or his own)

`Ideas 💡💡 -> Idea 💡`

and gives it with the _highest level of abstraction_
(possible at this time)
to the Driver ⌨️

note:
    lower level when needed

</div>

----

#### Mobber 💡

<div class="fragment">

Suggest ideas

`🤔 -> Ideas 💡💡`

</div>

----

Why should we bother **strictly respecting** these roles ?

<div class="fragment">

🤔 ➡ 💡 ➡ 🗣️💬 ➡ 🔀 ➡ 🗣️💬 ➡ 🧑‍💻 ➡ ⌨️ ➡ `{}`

Ideas 💡 must

be **clearly communicated** 🗣️💬

in order to to be **propery understood** (🔀 & 🧑‍💻)

to move from head 🤔 to code `{}`

</div>

----

Questions about these roles ?

----

#### Facilitator 🥷

<div class="fragment">

Try to make the team work well

`⚙️⚙ -> 🙂`

</div>

note:
  good facilitation is invisible

----

### Organic

Without strict rules

_Can_ work with group who :

* know how to work with each other
* are used to working together
* are used to working in mob programming
* have the same level

----

### Timer ⏳

AKA _Randori_

Rotation of roles based on a timer

* small turn
  * 3~6min/turn <!-- .element: class="fragment" -->
* big turn
  * each participant should have played both roles within 30min <!-- .element: class="fragment" -->
* only one person is the timekeeper <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

----

### Other modes

Another time

---

## Tools

* [MobTime](https://mobtime.hadrienmp.fr/): shared Timer ⏳
* Microsoft [Live Share](https://visualstudio.microsoft.com/fr/services/live-share/): realtime collaborative development (like Google Doc but for code)
  * 1 person must host <!-- .element: class="fragment" -->
  * the others can use a simple web browser <!-- .element: class="fragment" -->
  * best used with real installation <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->

----

* [tmate](https://tmate.io/): share terminal (for `vim`, `emacs`... users) <!-- .element: class="fragment" -->
* [ngrok](https://ngrok.com/): share local port througth internet <!-- .element: class="fragment" -->
* [mob.sh](https://mob.sh): small tool over git <!-- .element: class="fragment" -->
* [Mobster](http://mobster.cc/): local timer <!-- .element: class="fragment" -->
* JetBrains [Code With Me](https://www.jetbrains.com/code-with-me/) <!-- .element: class="fragment" -->

---

## External Resources

----

* Talks
  * 🇫🇷 [Ensemble Programming Toolbox (Thomas Carpaye et Hadrien Mens-Pellen)](https://youtu.be/c_oW0yJWveQ)
  * 🇫🇷 [Coder seul·e == vélocité optimale ? (Adrien Joly)](https://youtu.be/gjXk7VP1fuw)
  * 🇫🇷 [Le Mob programming : le meilleur moyen de partager les connaissances (Hugo Voisin)](https://youtu.be/H_3_t4ZxPfk)
* Definition
  * [Pair Programming](https://www.agilealliance.org/glossary/pairing/)
  * [Mob Programming](https://www.agilealliance.org/glossary/mob-programming/)

----

* Meetups
  * 🇫🇷 🏢 [Dojo Developpement Paris](https://www.meetup.com/fr-FR/dojo-developpement-paris/) 1/week
  * 🇫🇷 🛜 [Mob Programming Francophone](https://www.meetup.com/fr-FR/paris-mob-programming/) 1/month
  * 🇫🇷 🛜 [Software Crafters Lyon](https://www.meetup.com/fr-FR/software-craftsmanship-lyon/) mob session 1/month
  * 🇫🇷 [Okiwi - Software Craftsmanship Bordeaux](https://www.meetup.com/fr-FR/software-craftsmanship-bdx/)

----

* [Awesome list](https://github.com/mobtimeapp/awesome-mobbing)
* [Mob Programming website](https://mobprogramming.org/)
* [Ensemble RPG](https://ensemble-rpg.onrender.com/): play to discover roles by mobbing
* Do we loose time ?
  * [No](https://twitter.com/d_stepanovic/status/1379451260638785536)
  * [Explaining team flow (Michel Grootjans)](https://youtu.be/bhpQKA9XYcE)

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404age404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/mob_programming_roles">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/mob_programming_roles?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
