---
title: Fishbowl Mob Programming
verticalSeparator: ----
---

# Fishbowl Mob Programming

🪑🪑

---

## Objective

Discover a method of organization

<span class="fragment">very fun</span>

<span class="fragment">but only feasible in physics</span>

note:
    if you know how to adapt it at remote locations, i'm interested

---

- [Objective](#objective)
- ["Politic" Game](#politic-game)
- [Setup](#setup)
- [Rules](#rules)
- [Facilitator Tips](#facilitator-tips)

---

## "Politic" Game

<hr style="
border: none;
background-image: linear-gradient(to right, hsl(30,100%,50%), hsl(260,100%,60%), hsl(225,100%,50%));
height: 1em;
border-radius: 0.5em;
" />
<marquee behavior="alternate">⬆️</marquee>

<span class="fragment"><span class="fragment">Benevolent</span> Dictatorship</span>

<div class="fragment">

* Navigator 🔀 = <span class="fragment">_Benevolent_ Dictator 🧑‍⚖️🫅</span>
* Driver ⌨️ = <span class="fragment">Army / Executor 🫡🙇</span>
* Mobbers 💡 = <span class="fragment">Population 👥</span>

</div>

----

### An Unstable Regime

Disagree ⇒ <span class="fragment">(_Benevolent_) Coup d'État</span>

<div class="fragment">

To stay longer, ask the ~~population~~ _Mobbers_ 💡 for their ideas, then choose one

</div>

<div class="fragment">

Or ignore them, you are a dictator 😈

</div>

---

## Setup

<div class="fragment">

🪑🪑<br />2 chairs for the Navigator 🔀

</div>
<div class="fragment">

🪑🪑<br />2 chairs for the Driver ⌨️

</div>
<div class="fragment">

🪑🪑🪑🪑🪑🪑<br />As many chairs as needed for the Mobbers 💡

</div>
<div class="fragment">

A **video projector** or a large screen 🖵

</div>

----

<iframe class="r-stretch" src="./fishbowl/index.html"></iframe>

---

## Rules

----

For each role

There **must always** be _at least_ **1 chair available**🪑

----

When someone wants to take a role :

1. <span class="fragment">Sit in the chair of that role 🪑</span>
2. <span class="fragment">The previous person must return ➡️ with the Mobbers 💡</span>

----

When someone wants to stop a role :

1. Just return ➡️ with the Mobbers 💡

----

When both **chairs** in a role are **empty** 🪑🪑

<div class="fragment">

The group is **stuck** ⚠️

</div>

<div class="fragment">

So it's often a good idea to :

1. **take a break** ⏸️ <!-- .element: class="fragment" -->
2. then _slow down_ ⏳ <!-- .element: class="fragment" -->
3. with _smaller steps_ 🎯 <!-- .element: class="fragment" -->
4. and maybe _rollback_ 🔙 <!-- .element: class="fragment" -->
5. and maybe _make a plan_ 🗺️ <!-- .element: class="fragment" -->
6. … <!-- .element: class="fragment" -->

</div>

----

When someone has a **question** ❓ to ask

/ does not understand something :

1. <span class="fragment">stand up</span>
2. <span class="fragment">the group must stop until the question is answered</span>

---

## Facilitator Tips

----

Some people take the role of Navigator 🔀 too often ?

Introduce a totem 🙊 that prevents the owner from taking on this role while we have it <!-- .element: class="fragment" -->

When a Navigator 🔀 is revoked or leaves on its own, give the totem 🙊 <!-- .element: class="fragment" -->

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/mob_programming_roles">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/mob_programming_roles?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
