---
title: Facilitator Tips Mob Programming
verticalSeparator: ----
---

# Facilitator Tips Mob Programming

note:
    What was said previously ?

---

## Facilitator 🥷 role

Try to make the team work well <!-- .element: class="fragment" -->

----

Make them **feel at ease**

----

Questions ?

🤡 <!-- .element: class="fragment" -->

---

## Objective

Give tips for facilitating a group

----

### Minimum Framework

Try tips incrementally <!-- .element: class="fragment" -->

* **Retro** ➰🔙 _Mandatory_ <!-- .element: class="fragment" -->
* **Break** ⏸️ <!-- .element: class="fragment" -->
* **Strong Style** 👥 _Recommended_ <!-- .element: class="fragment" -->

---

<style>
.toc-2-columns ul {
    column-count: 2;
}
</style>
<div class="toc-2-columns">

- [Facilitator 🥷 role](#facilitator-role)
- [Objective](#objective)
- [Retro ➰🔙](#retro-)
- [Encourage 🙌](#encourage-)
- [Mobility Principle 👣](#mobility-principle-)
- [Safety ⛑️](#safety-️)
- [Facilitate by example](#facilitate-by-example)
- [Egoless Programming](#egoless-programming)
- [Situations](#situations)
- [Random Tips](#random-tips)
- [Rotate 🔁](#rotate-)
- [Silence 🙊](#silence-)
- [Timebox ⏱️](#timebox-️)
- [How to choose the Hoster 🧑‍🔧 ?](#how-to-choose-the-hoster-)
- [Experiment 🧪](#experiment-)
- [Enjoy 😃](#enjoy-)
- [External Resources](#external-resources)

</div>

---

## Retro ➰🔙

----

### Short Retro ⏸️➰🔙

Often

* After each big turn <!-- .element: class="fragment" -->
* Every 30min <!-- .element: class="fragment" -->
* Before/After a break <!-- .element: class="fragment" -->

----

#### Ask

* How do they **feel** ?
  * Not what they **think** about the solution 🚫🧠 <!-- .element: class="fragment" -->
  * Which **emotion** ? 😃 😢 😱 😠 <!-- .element: class="fragment" -->
  * I **pass** 🙅 <!-- .element: class="fragment" -->
  * How do you make them **feel at ease** ? 🫥➡️😃 <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->
* Do they want to **change** anything in the **organization** ? <!-- .element: class="fragment" -->
* …

Do **not argue** about the solution <!-- .element: class="fragment" -->

note:
    ask for emotion => get what they think ;
    changing timer duration ;
    changing mode ;
    changing constraint ;

----

What can be done to ensure they **leave satisfied** ? 😃

* Question ❓ about the solution
  * Not the moment ⏸️➰ <!-- .element: class="fragment" -->
  * How do you make them **feel at ease** ? 🫥➡️😃 <!-- .element: class="fragment" -->
  * Encourage questions during iteration ⏯️➰❓ <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->

note:
    maybe not the best moment ;
    idea 💡 ;
    target 🎯 ;
    change organization ;
    not know ok to ask question ;
    timebox 🗣️ ;

----

Which **target** do they want to achieve in the **next iteration** ? ⏭️➰🎯

note:
    maybe not the best moment

----

### Long Retro ⏹️➿🔙

* At the end of the session
* Ask
  * What did they **learn** ? 🧑‍🎓 <!-- .element: class="fragment" -->
  * What did they **like** ? 😃 <!-- .element: class="fragment" -->
  * What did they **dislike** ? 😒 <!-- .element: class="fragment" -->
  * What can be **improved** ? 🤔 <!-- .element: class="fragment" -->
  * …

---

## Encourage 🙌

But don't force 🚫💪

_It's called **consent**_ <!-- .element: class="fragment" -->

* Participating <!-- .element: class="fragment" -->
* Taking a role <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    it has a name : it's called consent ;

---

## Mobility Principle 👣

* Arriving late 👣 is ok <!-- .element: class="fragment" -->
* Going away 👣 is ok
  * break <!-- .element: class="fragment" -->
  * interruption
    * emergency 🔥 <!-- .element: class="fragment" -->
    * phone call 📳 <!-- .element: class="fragment" -->
    * delivery 📦 <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    open forum ;
    be courteous : let them know ;

---

## Safety ⛑️

People need to **feel safe** in the group

* Psychologically & Emotionnally
  * Right to make **mistakes** <!-- .element: class="fragment" -->
  * **Support** / mutual aid <!-- .element: class="fragment" -->
  * **Open-Mindedness** <!-- .element: class="fragment" -->
  * **Listen** <!-- .element: class="fragment" -->
  * **Courage** <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->

note:
    try ;
    learn together ;
    learn from each other ;
    courage to experiment ;
    courage to say when feeling not good ;
    courage to say when don't understand ;

---

## Facilitate by example

note:
    show your weakness ;

---

## Egoless Programming

> You are not your code <!-- .element: class="fragment" -->

> Treat people who know less than you
> with respect, deference, and patience <!-- .element: class="fragment" -->

<div class="fragment">

> Critique code instead of people :
> be kind to the coder,
> not to the code

_The Psychology of Computer Programming_ 📖 (Jerry Weinberg, 1971)

</div>

---

## Situations

---

### Some Mobbers 💡 don't speak 🙊 ?

<div class="fragment">

Whisper to them or go elsewhere and ask :

* Do they **feel at ease** ? <!-- .element: class="fragment" -->
* **What** do they **understand** ? <!-- .element: class="fragment" -->
* What are **their ideas ?** <!-- .element: class="fragment" -->
  * If they have any, **encourage** them to **suggest it** <!-- .element: class="fragment" -->
  * Else, **encourage** them to **take some role** <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->
* …

</div>

---

### Some don't dare to assume a role ?

----

Remind them that :

* the roles are pressure-free <!-- .element: class="fragment" -->
* we are not here to judge 🧑‍⚖️ but to solve a problem <!-- .element: class="fragment" -->
* it is best to let the group carry you along <!-- .element: class="fragment" -->
* …

----

#### Driver ⌨️

Only has to :

* type what the Navigator 🔀 requests <!-- .element: class="fragment" -->
* must not decide <!-- .element: class="fragment" -->
* ask Question ❓ while they doesn't know what to type <!-- .element: class="fragment" -->
* …

----

#### Navigator 🔀

Only has to :

* ask and collect Mobbers 💡 ideas <!-- .element: class="fragment" -->
* choose one <!-- .element: class="fragment" -->
* …

note:
    moderator

----

##### Abstraction

Talk with the **highest level of abstraction**

(_possible at this time_)

note:
    faster ;
    test with 3 ;
    fake it ;
    extract function ;
    say the keys on the keyboard ;

----

##### Line number

Use line number <!-- .element: class="fragment" -->

Use half line number to create a new one <!-- .element: class="fragment" -->

----

```python
1 def foo(bar):
2     return bar
```

<div class="fragment">

```python
1 def foo(bar):
2     if bar == "foo": return "baz"
3     return bar
```

</div>

note:
    Navigator 🔀 Fake it ! ;
    Driver ⌨️ How ? ;
    Navigator 🔀 Add an `if` in `1.5` ;
    Driver ⌨️ Ok ! ;

---

### Someone feels lost ?

----

#### Ensure that everyone understand

* What should be done
  * on the whole ? <!-- .element: class="fragment" -->
  * in the next step ? <!-- .element: class="fragment" -->
  * … <!-- .element: class="fragment" -->
* What has been done ? <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

----

#### Smaller target 🎯

----

#### Make a ToDo List 📝

* Prioritize <!-- .element: class="fragment" -->
* Divide the current step
  * as small as needed
  * to make it understandable to everyone <!-- .element: class="fragment" -->
* Stay focused on the current task <!-- .element: class="fragment" -->
* Note ideas 💡
  * that are not needed to the current task <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

----

#### Remind the target 🎯

----

#### Take breaks ⏸️

* Communication is exhausting <!-- .element: class="fragment" -->
* Take breaks **before** feeling tired <!-- .element: class="fragment" -->
* Try **Pomodoro** 🍅 <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    take sufficiently long breaks

---

### Someone doesn't respect the role ?

* Remind the role <!-- .element: class="fragment" -->
* Remember that we **respect** a framework for the **well-being** of the **group** <!-- .element: class="fragment" -->
* They can **propose changes** to the framework, but the _group must agree_ <!-- .element: class="fragment" -->
* If they continue not to **respect** the **well-being** of the **group**, they must **leave** the group ➡️🚪 <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    i need to improve ;

---

### Someone talks too much 🗣️ ?

* Remind the target 🎯 <!-- .element: class="fragment" -->
* Remind the role <!-- .element: class="fragment" -->
* Rotate the role <!-- .element: class="fragment" -->
* Reminder that this is a working **group** <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

note:
    i need to improve ;

---

### Feel stucks ?

So it's often a good idea to :

1. **take a break** ⏸️ <!-- .element: class="fragment" -->
2. then _slow down_ ⏳ <!-- .element: class="fragment" -->
3. with _smaller steps_ 🎯 <!-- .element: class="fragment" -->
4. and maybe _rollback_ 🔙 <!-- .element: class="fragment" -->
5. and maybe _make a plan_ 🗺️ <!-- .element: class="fragment" -->
6. … <!-- .element: class="fragment" -->

---

## Random Tips

---

## Rotate 🔁

Often

---

## Silence 🙊

* Short are good ➡ 🤔 <!-- .element: class="fragment" -->
* Too long are bad
  * Distracted 📱 <!-- .element: class="fragment" -->
  * Passive 😴 <!-- .element: class="fragment" -->
  * Stuck 🤷 <!-- .element: class="fragment" -->
  * Frustrated 😖 <!-- .element: class="fragment" -->
* … <!-- .element: class="fragment" -->

----

A well-functioning group is aligned with the target 🎯

---

## Timebox ⏱️

When jumping in the unknown 🕳️

---

## How to choose the Hoster 🧑‍🔧 ?

* With the best computer 💻 / connection 🛜 <!-- .element: class="fragment" -->
* That will be availlable for a long time <!-- .element: class="fragment" -->

---

## Experiment 🧪

⚗️🧫🔬

* new idea <!-- .element: class="fragment" -->
* new tool <!-- .element: class="fragment" -->
* new organisation <!-- .element: class="fragment" -->
* new constraint <!-- .element: class="fragment" -->

---

## Enjoy 😃

Have fun 🥳

note:
    it is unusual to have jobs that allow this type of organization ;

---

## External Resources

----

* Talks
  * [🇫🇷 Ensemble Programming Toolbox (Thomas Carpaye et Hadrien Mens-Pellen)](https://youtu.be/c_oW0yJWveQ)
* [🇫🇷 Qu'est-ce qu'un facilitateur ? (Le Laptop)](https://youtu.be/lb4AwgPvyu0)
* [🇫🇷 Les étapes de la facilitation (Le Laptop)](https://youtu.be/SEQJn1KgiKw)
* [Ensemble RPG](https://ensemble-rpg.onrender.com/): play to discover roles by mobbing
* [Good facilitation becomes invisible because …](https://twitter.com/malk_zameth/status/1657079894310805511)

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/mob_programming_roles">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/mob_programming_roles?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
