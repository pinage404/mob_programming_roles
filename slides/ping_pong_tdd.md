---
title: Ping Pong TDD 🏓 Mob Programming
verticalSeparator: ----
---

# Ping Pong TDD 🏓 Mob Programming

Works well with Pair Programming

---

## Objective

Discover a method of organization based on tests

---

- [Objective](#objective)
- [TDD](#tdd)
- [Ping Pong TDD 🏓](#ping-pong-tdd-)

---

## TDD

----

Test Driven Development

note:
    Development Driven by the Tests

----

```mermaid
%%{ init: { 'theme':'dark', 'flowchart': { 'curve': 'basis' } } }%%
flowchart TB
    %% relations
    start -->|test a new behavior| Red
    Red -->|make it pass| Green
    Green -->|test a new behavior| Red
    Green -->|"refactor\nmake it better\nmake it fast (if needed)"| Green
    Green --> finish

    %% styling
    start((start))
    finish((finish))

    classDef Red fill:red,color:black,stroke:black
    class Red Red

    classDef Green fill:green,color:black,stroke:black
    class Green Green
```

---

## Ping Pong TDD 🏓

----

* 👩🏻‍💻 Alice
* 👨🏾‍💻 Bob
* 👩🏿‍💻 Charlie

----

Turn 1

```mermaid
%%{ init: { 'theme':'dark', 'flowchart': { 'curve': 'basis' } } }%%
flowchart LR
    %% relations

    subgraph Red
        direction LR

        RedDriver --- RedA
        RedNavigator --- RedB
        RedMobber --- RedC
    end

    subgraph Green
        direction LR

        GreenDriver --- GreenB
        GreenNavigator --- GreenC
        GreenMobber --- GreenA
    end

    start((" ")) -->|test a new behavior| Red
    Red -->|"make it pass\n🔁"| Green
    Green -->|refactor| Green
    Green --> finish((" "))

    %% styling
    RedDriver["Driver ⌨️"]
    RedNavigator["Navigator 🔀 _"]
    RedMobber["Mobber 💡 _"]

    GreenDriver["Driver ⌨️"]
    GreenNavigator["Navigator 🔀 _"]
    GreenMobber["Mobber 💡 _"]

    RedA["👩🏻‍💻 Alice"]
    RedB["👨🏾‍💻 Bob"]
    RedC["👩🏿‍💻 Charlie"]

    GreenA["👩🏻‍💻 Alice"]
    GreenB["👨🏾‍💻 Bob"]
    GreenC["👩🏿‍💻 Charlie"]

    classDef Dark fill:#0008,color:white,stroke:transparent
    class RedDriver,RedNavigator,RedMobber,GreenDriver,GreenNavigator,GreenMobber,RedA,RedB,RedC,GreenA,GreenB,GreenC Dark

    classDef Red fill:darkred,color:white
    class Red Red

    classDef Green fill:darkgreen,color:white
    class Green Green
```

----

Turn 2

```mermaid
%%{ init: { 'theme':'dark', 'flowchart': { 'curve': 'basis' } } }%%
flowchart LR
    %% relations

    subgraph Red
        direction LR

        RedDriver --- RedB
        RedNavigator --- RedC
        RedMobber --- RedA
    end

    subgraph Green
        direction LR

        GreenDriver --- GreenC
        GreenNavigator --- GreenA
        GreenMobber --- GreenB
    end

    start((" ")) -->|test a new behavior| Red
    Red -->|"make it pass\n🔁"| Green
    Green -->|refactor| Green
    Green --> finish((" "))

    %% styling
    RedDriver["Driver ⌨️"]
    RedNavigator["Navigator 🔀 _"]
    RedMobber["Mobber 💡 _"]

    GreenDriver["Driver ⌨️"]
    GreenNavigator["Navigator 🔀 _"]
    GreenMobber["Mobber 💡 _"]

    RedA["👩🏻‍💻 Alice"]
    RedB["👨🏾‍💻 Bob"]
    RedC["👩🏿‍💻 Charlie"]

    GreenA["👩🏻‍💻 Alice"]
    GreenB["👨🏾‍💻 Bob"]
    GreenC["👩🏿‍💻 Charlie"]

    classDef Dark fill:#0008,color:white,stroke:transparent
    class RedDriver,RedNavigator,RedMobber,GreenDriver,GreenNavigator,GreenMobber,RedA,RedB,RedC,GreenA,GreenB,GreenC Dark

    classDef Red fill:darkred,color:white
    class Red Red

    classDef Green fill:darkgreen,color:white
    class Green Green
```

----

Turn 3

```mermaid
%%{ init: { 'theme':'dark', 'flowchart': { 'curve': 'basis' } } }%%
flowchart LR
    %% relations

    subgraph Red
        direction LR

        RedDriver --- RedC
        RedNavigator --- RedA
        RedMobber --- RedB
    end

    subgraph Green
        direction LR

        GreenDriver --- GreenA
        GreenNavigator --- GreenB
        GreenMobber --- GreenC
    end

    start((" ")) -->|test a new behavior| Red
    Red -->|"make it pass\n🔁"| Green
    Green -->|refactor| Green
    Green --> finish((" "))

    %% styling
    RedDriver["Driver ⌨️"]
    RedNavigator["Navigator 🔀 _"]
    RedMobber["Mobber 💡 _"]

    GreenDriver["Driver ⌨️"]
    GreenNavigator["Navigator 🔀 _"]
    GreenMobber["Mobber 💡 _"]

    RedA["👩🏻‍💻 Alice"]
    RedB["👨🏾‍💻 Bob"]
    RedC["👩🏿‍💻 Charlie"]

    GreenA["👩🏻‍💻 Alice"]
    GreenB["👨🏾‍💻 Bob"]
    GreenC["👩🏿‍💻 Charlie"]

    classDef Dark fill:#0008,color:white,stroke:transparent
    class RedDriver,RedNavigator,RedMobber,GreenDriver,GreenNavigator,GreenMobber,RedA,RedB,RedC,GreenA,GreenB,GreenC Dark

    classDef Red fill:darkred,color:white
    class Red Red

    classDef Green fill:darkgreen,color:white
    class Green Green
```

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/mob_programming_roles">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/mob_programming_roles?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
