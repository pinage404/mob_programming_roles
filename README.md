# Mob Programming

## Modules

* [Mob 101](./slides/basics.md)
  * Format : Theory + Practice
  * Duration : ~2h
  * Objective : Align everyone with the mob basics
* [Facilitator tips](./slides/facilitator_tips.md)
  * Format : Theory + Practice
  * Duration : ~2h minimum
  * Objective : Give tips for facilitating a group
* Alternative modes
  * [Fishbowl](./slides/fishbowl.md)
    * Format : Theory + Practice
    * Duration : 1h30~2h
    * Objective : Discover a method of organization, very fun but only feasible in physics
    * Constraint : on-site
  * [Ping Pong TDD](./slides/ping_pong_tdd.md)
    * Format : Theory + Practice
    * Duration : ~2h
    * Objective : Discover a method of organization based on tests
* [Pair Programming](./exercices.md#pair-programming)
  * Format : Practice
  * Duration : ~2h
  * Objective : Illustrate what is important in Pair Programming with constrained exercises
  * Constraint : on-site

## Dependencies

```mermaid
flowchart LR
    101[Mob 101] --> Facilitator[Facilitator tips]
    101[Mob 101] --> fishbowl[Fishbowl]
    ping_ping[Ping Pong TDD]
    pair[Pair Programming]
```
