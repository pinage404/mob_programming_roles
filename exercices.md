# Exercices

## Mob Programming

* 🪑
* ⏳
* 🧙🫅🧝🧌
* 🏓

## Pair Programming

* 🏓
* 3 * 40m
  * 🧟🤤🧠🙊
  * 🧑‍🦯😎🙈
  * 🧏🙊🙉
